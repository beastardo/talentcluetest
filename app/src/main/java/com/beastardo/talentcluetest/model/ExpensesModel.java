package com.beastardo.talentcluetest.model;

public class ExpensesModel {
    private Long mTimestamp;
    private String mFormattedTimestamp;
    private String mReason;
    private Float mAmount;

    public ExpensesModel() {

    }

    public Long getTimestamp() {
        return this.mTimestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.mTimestamp = timestamp;
    }

    public String getFormattedTimestamp() {
        return mFormattedTimestamp;
    }

    public void setFormattedTimestamp(String formattedTimestamp) {
        mFormattedTimestamp = formattedTimestamp;
    }

    public String getReason() {
        return this.mReason;
    }

    public void setReason(String reason) {
        this.mReason = reason;
    }

    public Float getAmount() {
        return this.mAmount;
    }

    public void setAmount(Float amount) {
        this.mAmount = amount;
    }
}
