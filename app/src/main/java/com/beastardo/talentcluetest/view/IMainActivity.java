package com.beastardo.talentcluetest.view;

public interface IMainActivity {
    int isListVisible();
    void showExpensesList();
}
