package com.beastardo.talentcluetest.view;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beastardo.talentcluetest.R;
import com.beastardo.talentcluetest.presenter.AddExpensePresenterImpl;
import com.beastardo.talentcluetest.presenter.IAddExpensePresenter;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddExpenseActivity extends AppCompatActivity implements IAddExpenseActivity {

    @BindView(R.id.add_date_picker)
    EditText mDatePicker;

    @BindView(R.id.add_reason_editText)
    EditText mReasonInput;

    @BindView(R.id.add_amount_editText)
    EditText mAmountInput;

    @BindView(R.id.save_button)
    Button mAddExpenseButton;

    @BindView(R.id.cancel_button)
    TextView mCancelButton;

    IAddExpensePresenter mExpensePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        ButterKnife.bind(this);
        mExpensePresenter = new AddExpensePresenterImpl(this);

        this.setListeners();
    }

    private void setListeners() {
        this.mDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, day);
                        AddExpenseActivity.this.mExpensePresenter.setSelectedDate(calendar.getTimeInMillis());
                    }
                });
                newFragment.show(AddExpenseActivity.this.getSupportFragmentManager(), "datePicker");
            }
        });

        this.mAddExpenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddExpenseActivity.this.mExpensePresenter.saveExpense();
            }
        });

        this.mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddExpenseActivity.this.mExpensePresenter.cancelExpense();
            }
        });
    }

    @Override
    public void setDateText(String date) {
        this.mDatePicker.setText(date);
    }

    @Override
    public String getReasonText() {
        return this.mReasonInput.getText().toString();
    }

    @Override
    public String getAmount() {
        return this.mAmountInput.getText().toString();
    }

    @Override
    public void closeActivity() {
        this.finish();
    }

    @Override
    public void showInvalidDataToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
