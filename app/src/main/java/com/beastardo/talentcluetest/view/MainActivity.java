package com.beastardo.talentcluetest.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.beastardo.talentcluetest.R;
import com.beastardo.talentcluetest.adapter.MainAdapter;
import com.beastardo.talentcluetest.presenter.IMainPresenter;
import com.beastardo.talentcluetest.presenter.MainPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IMainActivity {

    @BindView(R.id.main_scrollView)
    NestedScrollView mainScrollView;

    @BindView(R.id.main_recyclerView)
    RecyclerView mainRecyclerView;

    @BindView(R.id.no_expenses_textView)
    TextView mainTextView;

    @BindView(R.id.add_expense_button)
    Button addExpenseButton;

    IMainPresenter mMainPresenter;
    MainAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        this.mMainPresenter = new MainPresenterImpl(this);
        this.initRecyclerView();
        this.addExpenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddExpenseActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mAdapter.updateList(this.mMainPresenter.getExpensesList());
        this.mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showExpensesList() {
        this.mainScrollView.setVisibility(View.VISIBLE);
        this.mainTextView.setVisibility(View.GONE);
    }

    @Override
    public int isListVisible() {
        return this.mainScrollView.getVisibility();
    }

    private void initRecyclerView() {
        this.mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.mainRecyclerView.setHasFixedSize(true);
        this.mAdapter = new MainAdapter(this.mMainPresenter.getExpensesList());
        this.mainRecyclerView.setAdapter(this.mAdapter);
        ViewCompat.setNestedScrollingEnabled(this.mainRecyclerView, true);
    }
}
