package com.beastardo.talentcluetest.view;

public interface IAddExpenseActivity {
    void setDateText(String date);
    void closeActivity();
    String getReasonText();
    String getAmount();
    void showInvalidDataToast(String text);
}
