package com.beastardo.talentcluetest.presenter;

import com.beastardo.talentcluetest.model.ExpensesModel;

import java.util.List;

public interface IMainPresenter {
    List<ExpensesModel> getExpensesList();
}
