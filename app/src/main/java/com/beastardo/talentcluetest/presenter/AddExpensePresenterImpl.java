package com.beastardo.talentcluetest.presenter;

import com.beastardo.talentcluetest.model.ExpensesModel;
import com.beastardo.talentcluetest.utils.ExpensesListManager;
import com.beastardo.talentcluetest.view.IAddExpenseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddExpensePresenterImpl implements IAddExpensePresenter {

    private SimpleDateFormat mFormat = new SimpleDateFormat("dd/MM/yyyy");

    private IAddExpenseActivity mExpenseActivity;
    private ExpensesModel mModel;

    public AddExpensePresenterImpl(IAddExpenseActivity activity) {
        this.mExpenseActivity = activity;
        this.mModel = new ExpensesModel();
    }

    @Override
    public void setSelectedDate(Long selectedDateInMillis) {
        this.mModel.setTimestamp(selectedDateInMillis);
        this.mModel.setFormattedTimestamp(this.parseTimestampToString());
        this.mExpenseActivity.setDateText(this.mModel.getFormattedTimestamp());
    }

    @Override
    public void saveExpense() {
        String reason = this.mExpenseActivity.getReasonText();
        String amountString = this.mExpenseActivity.getAmount();

        if (reason.isEmpty() || amountString.isEmpty()) {
            this.mExpenseActivity.showInvalidDataToast("El concepto o el importe son incorrectos.");
        } else {
            if (this.mModel.getTimestamp() == null) {
                this.setDefaultDate();
            }
            float amount = Float.parseFloat(amountString);
            this.mModel.setReason(reason);
            this.mModel.setAmount(amount);
            ExpensesListManager.addNewExpense(this.mModel);
            this.mExpenseActivity.closeActivity();
        }
    }

    @Override
    public void cancelExpense() {
        if (this.mExpenseActivity != null) {
            this.mExpenseActivity.closeActivity();
        }
    }

    private void setDefaultDate() {
        this.mModel.setTimestamp(Calendar.getInstance().getTimeInMillis());
        this.mModel.setFormattedTimestamp(this.parseTimestampToString());
    }

    private String parseTimestampToString() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(this.mModel.getTimestamp());
        return mFormat.format(calendar.getTime());
    }
}
