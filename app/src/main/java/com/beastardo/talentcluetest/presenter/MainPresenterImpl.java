package com.beastardo.talentcluetest.presenter;

import android.view.View;

import com.beastardo.talentcluetest.model.ExpensesModel;
import com.beastardo.talentcluetest.utils.ExpensesListManager;
import com.beastardo.talentcluetest.view.IMainActivity;

import java.util.List;

public class MainPresenterImpl implements IMainPresenter {
    IMainActivity mMainActivity;

    public MainPresenterImpl(IMainActivity activity) {
        this.mMainActivity = activity;
    }

    @Override
    public List<ExpensesModel> getExpensesList() {
        List<ExpensesModel> list = ExpensesListManager.getExpensesList();
        if ((list.size() > 0) && (this.mMainActivity.isListVisible() == View.GONE)) {
            this.mMainActivity.showExpensesList();
        }

        return list;
    }
}
