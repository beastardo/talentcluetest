package com.beastardo.talentcluetest.presenter;

public interface IAddExpensePresenter {
    void setSelectedDate(Long selectedDateInMillis);
    void saveExpense();
    void cancelExpense();
}
