package com.beastardo.talentcluetest.utils;

import com.beastardo.talentcluetest.model.ExpensesModel;

import java.util.ArrayList;
import java.util.List;

public class ExpensesListManager {
    private static List<ExpensesModel> mExpensesList;

    public static List<ExpensesModel> getExpensesList() {
        if (mExpensesList == null) {
            mExpensesList = new ArrayList<>();
        }

        return mExpensesList;
    }

    public static void addNewExpense(ExpensesModel model) {
        if (mExpensesList.size() == 0) {
            mExpensesList.add(model);
        } else {
            sortList(model);
        }
    }

    private static void sortList(ExpensesModel model) {
        boolean isModelAdded = false;
        List<ExpensesModel> auxList = new ArrayList<>();
        for (int i = 0; i < mExpensesList.size(); i++) {
            if (!isModelAdded) {
                if (model.getTimestamp() >= mExpensesList.get(i).getTimestamp()) {
                    auxList.add(model);
                    isModelAdded = true;
                }
            }
            auxList.add(mExpensesList.get(i));
        }

        if (!isModelAdded) {
            auxList.add(model);
        }

        mExpensesList = auxList;
    }
}
