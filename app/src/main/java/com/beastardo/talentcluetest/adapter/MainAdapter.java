package com.beastardo.talentcluetest.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.beastardo.talentcluetest.R;
import com.beastardo.talentcluetest.model.ExpensesModel;

import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<ExpensesModel> mExpensesList;

    public MainAdapter(List<ExpensesModel> list) {
        this.mExpensesList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.main_viewholder_layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setValues(this.mExpensesList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.mExpensesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date, reason, amount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.date = itemView.findViewById(R.id.expenses_date);
            this.reason = itemView.findViewById(R.id.expenses_reason);
            this.amount = itemView.findViewById(R.id.expenses_amount);
        }

        public void setValues(ExpensesModel model) {
            this.date.setText(model.getFormattedTimestamp());
            this.reason.setText(model.getReason());
            this.amount.setText(String.format("%.2f", model.getAmount()) + "€");
        }
    }

    public void updateList(List<ExpensesModel> list) {
        this.mExpensesList = list;
    }
}
